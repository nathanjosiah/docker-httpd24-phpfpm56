FROM centos:centos7
MAINTAINER Nathan Smith <nathanjosiah@gmail.com>

RUN yum -y update; yum clean all

RUN yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm; yum clean all
RUN yum -y install http://rpms.famillecollet.com/enterprise/remi-release-7.rpm; yum clean all
RUN yum -y install https://centos7.iuscommunity.org/ius-release.rpm; yum clean all

RUN yum -y install sudo git vim pwgen supervisor bash-completion openssh-server psmisc tar; yum clean all;
RUN yum -y install httpd24u httpd24u-{filesystem,tools} mod24u_ssl; yum clean all
RUN yum -y install php56 php56-php-{pdo,mysqlnd,bcmath,devel,gd,mbstring,mcrypt,pecl-imagick,pecl-redis,soap,tidy,xml,pear,opcache,fpm,xdebug} --enablerepo=remi; yum clean all

RUN yum -y install https://dl-ssl.google.com/dl/linux/direct/mod-pagespeed-stable_current_x86_64.rpm; yum clean all

ADD ./files/scripts /scripts/
RUN chmod -R 755 /scripts/

ADD ./files/supervisord.conf /etc/supervisord.conf

ADD ./files/config/php/fpm-www.conf /opt/remi/php56/root/etc/php-fpm.d/www.conf
ADD ./files/config/php/xdebug.ini /xdebug.ini
ADD ./files/config/httpd/php-fpm.conf /etc/httpd/conf.d/php-fpm.conf

RUN mkdir /var/run/sshd

EXPOSE 80
EXPOSE 443
EXPOSE 22