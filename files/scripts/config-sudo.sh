#! /bin/bash

if [ $(grep %apache /etc/sudoers | wc -l) -lt 1 ]; then
	echo %sudo	ALL=NOPASSWD: ALL >> /etc/sudoers
	echo %apache	ALL=NOPASSWD: ALL >> /etc/sudoers
	sed -i '/requiretty/s/^/#/' /etc/sudoers
fi