#! /bin/bash

# Configure timezone
if [ $(grep $PHP_TIMEZONE | wc -l) -lt 1 ]; then
	sed -i 's|;date.timezone\s=|date.timezone = "'$PHP_TIMEZONE'"|g' /opt/remi/php56/root/etc/php.ini
	sed -i 's|short_open_tag = Off|short_open_tag = On|g' /opt/remi/php56/root/etc/php.ini
fi

# Configure xdebug
if [ "$ENABLE_XDEBUG" -eq "1" ] && [ ! -f "/opt/remi/php56/root/etc/php.d/45-xdebug.ini" ]; then
	mv /xdebug.ini /opt/remi/php56/root/etc/php.d/45-xdebug.ini
fi

# Configure Redis
if [ -n "$REDIS_ENDPOINT" ]; then

	REDIS_PHP_VALUE="tcp://$REDIS_ENDPOINT?weight=1"
	if [ -n "$REDIS_PASSWORD" ]; then
		REDIS_PHP_VALUE="${REDIS_PHP_VALUE}&auth=${REDIS_PASSWORD}"
	fi

	echo php_value[session.save_handler] = redis >> /opt/remi/php56/root/etc/php-fpm.d/www.conf
	echo php_value[session.save_path] = \"$REDIS_PHP_VALUE\" >> /opt/remi/php56/root/etc/php-fpm.d/www.conf
fi