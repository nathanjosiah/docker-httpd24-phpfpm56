#! /bin/bash

if [ ! -f "/var/www/html/composer.lock" ]; then
	if [ ! -d "/root/.ssh" ]; then
		mkdir /root/.ssh
		chmod 700 /root/.ssh
	fi
	if [ ! -f "/root/.ssh/known_hosts" ]; then
		touch /root/.ssh/known_hosts
		chmod 600 /root/.ssh/known_hosts
	fi

	# Get the key that git normally would and add it to our trusted hosts. Otherwise, git will try to ask for input to accept the host.
	ssh-keyscan $PROJECT_GIT_DOMAIN >> /root/.ssh/known_hosts

	PREV_PWD=$PWD
	cd /var/www/html
	git init .
	git remote add origin $PROJECT_GIT_URL
	ssh-agent bash -c 'ssh-add /keys/repo.key; git pull origin master;'
	cd $PREV_PWD
fi