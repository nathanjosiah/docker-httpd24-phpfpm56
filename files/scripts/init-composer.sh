#! /bin/bash

if [ ! -d "/var/www/html/vendor" ]; then
	if [ ! -f "/var/www/html/composer.phar" ]; then
		scl enable php56 "curl http://getcomposer.org/installer | php"
		mv composer.phar /var/www/html/
	fi

	ssh-agent bash -c 'ssh-add /keys/repo.key;scl enable php56 "cd /var/www/html;php /var/www/html/composer.phar install --no-dev"'
fi