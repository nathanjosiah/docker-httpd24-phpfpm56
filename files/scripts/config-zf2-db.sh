#! /bin/bash

if [ ! -d "/var/www/html/config/" ]; then
	return
fi

cat << CONFIG > /var/www/html/config/autoload/db.php
<?php
	return [
	'db' => [
		'username' => '$DB_USER',
		'password' => '$DB_PASSWORD',
		'driver' => 'Pdo',
		'dsn' => 'mysql:dbname=$DB_DB;host=$DB_HOST;charset=utf8'
	]
];
CONFIG