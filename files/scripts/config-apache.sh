#! /bin/bash

if [ -f /etc/httpd/conf.d/autoindex.conf ]; then
	mv /etc/httpd/conf.d/autoindex.conf{,.DISABLED}
fi

if [ $(grep vhost\.d /etc/httpd/conf/httpd.conf | wc -l) -lt 1 ]; then
	echo 'EnableSendfile Off' >> /etc/httpd/conf/httpd.conf
	echo 'IncludeOptional vhost.d/*.conf' >> /etc/httpd/conf/httpd.conf
fi